# CS2 Player Stats

Extract all players stats in the given `.dem` files, using [akiver/cs-demo-analyzer](https://github.com/akiver/cs-demo-analyzer)

## Requirements:
  * Put your `.dem` files in the `demos/` directory

## Run:
### Option one: Python
  You need the `csda` binary on your system from [akiver/cs-demo-analyzer](https://github.com/akiver/cs-demo-analyzer)

  Install pip requirements:
  ```sh
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
  ```

  Run:
  ```sh
    python main.py
  ```

### Option Two: Docker Compose

  You need docker and docker-compose, nothing else.

  ```sh
    git clone https://gitlab.com/v1nte/cs2-players-stats
    docker-compose up
```
