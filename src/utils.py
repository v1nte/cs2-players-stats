import os
import json

from src.config import stats_to_extract, stats_as_set


def _save_to_json(data: list, filename: str = "players_stats.json") -> None:

    with open(filename, "w") as json_file:
        json.dump(data, json_file)


def create_directories():

    directories = ["jsons_files"]

    for dir in directories:
        if not os.path.exists(dir):
            os.makedirs(dir)
            print(f"Directory {dir} created")
        else:
            print(f"Directory {dir} already exists")


def load_data():
    json_directory = "jsons_files"
    json_files = os.listdir(json_directory)

    files_with_full_path = [os.path.join(json_directory, file) for file in json_files]

    key_to_extract = "players"

    data = [
        json.load(open(file_path, "r"))[key_to_extract]
        for file_path in files_with_full_path
        if key_to_extract in json.load(open(file_path, "r"))
    ]

    return data


def _formated_json(data: list) -> list:

    for d in data:
        for key, value in d.items():
            if "name" in value:
                d[key]["name"] = [name.strip("{}") for name in value["name"]]

    restructured_data = []
    for item in data:
        for steamid, values in item.items():
            restructured_dict = {
                "player": {
                    "steamid": steamid,
                    "name": values["name"][0].strip("{}"),
                },
                **{k: v for k, v in values.items() if k != "name"},
            }

            restructured_data.append(restructured_dict)

    for entry in restructured_data:
        player_info = entry.pop("player")

        stats = {k: v for k, v in entry.items() if isinstance(v, list)}

        averages = {k: sum(v) / len(v) for k, v in stats.items()}
        sums = {k: sum(v) for k, v in stats.items()}

        entry["player"] = player_info
        entry["average_stats"] = averages
        entry["sums_stats"] = sums
        entry["matches"] = stats

        for k in stats.keys():
            del entry[k]

    return restructured_data


def generate_json(data: list) -> list:
    full_players_stats = []

    for match in data:
        for steamid, value in match.items():
            id_to_find = steamid

            id_found = False

            for item in full_players_stats:
                if id_to_find in item:
                    for stat in stats_to_extract:
                        if stat in value:
                            if stat in stats_as_set:
                                item[id_to_find][stat].add(value[stat])
                            else:
                                item[id_to_find][stat].append(value[stat])

                    id_found = True
                    break

            if not id_found:
                new_item = {id_to_find: {}}
                for stat in stats_to_extract:
                    if stat in value:
                        if stat in stats_as_set:
                            new_item[id_to_find][stat] = {value[stat]}
                        else:
                            new_item[id_to_find][stat] = [value[stat]]

                full_players_stats.append(new_item)

    formated_data = _formated_json(full_players_stats)
    _save_to_json(formated_data)
    return formated_data
