from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.process_data import process_data

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get('/')
async def root():
    data = process_data()
    return data
