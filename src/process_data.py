import os
import json

from tqdm import tqdm

from src.extractor import data_extractor
from src.utils import create_directories, load_data, generate_json


def process_data():

    create_directories()

    data_extractor()

    raw_data = load_data()

    data = generate_json(raw_data)

    return data
