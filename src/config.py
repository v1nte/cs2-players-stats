stats_to_extract = [
    "name",
    "averageDamagePerRound",
    "killCount",
    "deathCount",
    "assistCount",
    "killDeathRatio",
    "averageKillPerRound",
    "headshotPercentage",
    "oneKillCount",
    "twoKillCount",
    "threeKillCount",
    "fourKillCount",
    "fiveKillCount",
    "hltvRating2",
]

stats_as_set = ["name"]
