import os
from tqdm import tqdm


def is_json_file_exists(dem_file, json_dir="jsons_files"):

    json_filename = os.path.join(json_dir, dem_file.replace(".dem", ".json"))

    return os.path.exists(json_filename)


def get_dem_files(directory):

    dem_files = [file for file in os.listdir(directory) if file.endswith(".dem")]

    return dem_files


def data_extractor():
    directory = "demos"
    out_directory = "jsons_files"
    dem_files = get_dem_files(directory)

    print("Extracting demos info")

    for raw_dem in tqdm(dem_files):
        if not is_json_file_exists(raw_dem):
            command = f"csda -demo-path={directory}/{raw_dem} -format=json -output={out_directory}"

            os.system(command)

        print(f"{raw_dem} Done with this one!")

    print("Done extracting demos info")
