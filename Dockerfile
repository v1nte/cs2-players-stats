FROM python:3.12.3-slim 


WORKDIR /app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt
RUN mv /app/src/csda /usr/local/bin/
RUN chmod +x /usr/local/bin/csda

CMD [ "python", "main.py" ]
